'use strict';

angular.module('appApp')
  .factory('api', ['$http', function ($http) {
    var api = {};

    api.getData = function (file) {
      return $http({
        method: 'GET',
        url: 'test-work/' + file + '.json',
        cache: true,
        headers: {'Accept': 'application/json'}
      });
    };

    api.postData = function (model) {
      return $http({
        method: 'POST',
        url: 'test-work/' + model,
        cache: true,
        headers: {'Accept': 'application/json'}
      });
    };

    return api;
  }]);
