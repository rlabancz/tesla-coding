'use strict';

/**
 * @ngdoc function
 * @name appApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the appApp
 */
angular.module('appApp')
  .controller('MainCtrl', ['$scope', '_', 'api', function ($scope, _, api) {

    $scope.optionsLoaded = false;
    $scope.dataLoaded = true;

    api.getData('options').then(function (result) {
      $scope.options = result.data.options;
      $scope.optionsLoaded = true;
      $scope.selectedModel = $scope.options[0];
    }, function (error) {
      console.log(error);
      $scope.optionsLoaded = true;

    });

    $scope.submit = function () {
      $scope.dataLoaded = false;

      api.getData('database').then(function (result) {
        var database = result.data.database;
        $scope.database = [];
        /* mock START */
        //TODO: mock response, should be done in backend
        var selectedModels = _.where(database, {model: $scope.selectedModel.value});
        _.forEach(selectedModels, function (selectedModel) {
          var configurations = selectedModel.configuration.split(';');
          var model = configurations.shift().split('|');
          var vehicle = {
            id: model[0],
            price: parseInt(model[2], 10),
            options: {},
            optionPrice: 0,
            optionList: []
          };
          _.forEach(configurations, function (configuration) {
            var option = configuration.split('|');
            vehicle.options[option[0]] = {
              code: $scope.selectedModel.options[option[0]].code,
              priceIndicator: option[1],
              price: option[2],
              name: $scope.selectedModel.options[option[0]].name
            };
          });
          $scope.database.push(vehicle);
        });
        $scope.filteredDatabase = angular.copy($scope.database);

        angular.forEach($scope.database, function (database, index) {
          $scope.currentData = database;
          $scope.currentIndex = index;

          angular.forEach($scope.selectedOptions, function (selectedOption) {
            if (!this.currentData.options[selectedOption.id]) {
              this.filteredDatabase.splice(this.currentIndex, 1);
              return;
            }
          }, $scope);
        });

        _.forEach($scope.filteredDatabase, function (filteredItem) {
          _.forEach(filteredItem.options, function (option) {
            this.optionPrice += parseInt(option.price, 10);
            this.optionList.push(option.name);
          }, filteredItem);
        });

        /* mock END */

        $scope.dataLoaded = true;
      }, function (error) {
        console.log(error);
        $scope.dataLoaded = true;
      });
    };

    $scope.selectedOptions = [];
    $scope.results = [];
  }]);
